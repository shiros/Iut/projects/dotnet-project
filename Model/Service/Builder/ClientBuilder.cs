﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Service.Builder
{
    using Model.Entity.Client;

    public class ClientBuilder
    {
        public static Client create(String lastname, String firstname)
        {
            Client client = new Client();
            client.Lastname = lastname;
            client.Firstname = firstname;

            return client;
        }

        public static List<Client> createList()
        {
            List<Client> clients = new List<Client>();
            clients.Add(create("Caillot", "Alexandre"));
            clients.Add(create("Hebert", "Florian"));

            return clients;
        }
    }
}
