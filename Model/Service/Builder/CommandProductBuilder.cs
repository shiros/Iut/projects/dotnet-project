﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Service.Builder
{
    using Model.Entity.Command;
    using Model.Entity.Product;

    public class CommandProductBuilder
    {
        public static CommandProduct create(Command command, Product product)
        {
            CommandProduct commandProduct = new CommandProduct();
            commandProduct.Command = command;
            commandProduct.CommandId = command.Id;
            commandProduct.Product = product;
            commandProduct.ProductId = product.Id;

            return commandProduct;
        }

        public static List<CommandProduct> createList(List<Command> commands, List<Product> products)
        {
            List<CommandProduct> commandProducts = new List<CommandProduct>();
            commandProducts.Add(create(commands.ElementAt(0), products.ElementAt(0)));
            commandProducts.Add(create(commands.ElementAt(1), products.ElementAt(1)));
            commandProducts.Add(create(commands.ElementAt(2), products.ElementAt(2)));
            commandProducts.Add(create(commands.ElementAt(3), products.ElementAt(3)));

            return commandProducts;
        }
    }
}
