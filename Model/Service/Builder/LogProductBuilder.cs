﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Service.Builder
{
    using Model.Entity.Product;

    public class LogProductBuilder
    {
        public static LogProduct create(String message, Product product)
        {
            LogProduct logProduct = new LogProduct();
            logProduct.Message = message;
            logProduct.Product = product;
            logProduct.ProductId = product.Id;

            return logProduct;
        }

        public static List<LogProduct> createList(List<Product> products)
        {
            List<LogProduct> logProducts = new List<LogProduct>();
            logProducts.Add(create("Message 1 LogProduct", products.ElementAt(0)));
            logProducts.Add(create("Message 2 LogProduct", products.ElementAt(1)));
            logProducts.Add(create("Message 3 LogProduct", products.ElementAt(2)));
            logProducts.Add(create("Message 4 LogProduct", products.ElementAt(3)));

            return logProducts;
        }
    }
}
