﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Repository.Product
{
    using Entity.Product;
    using Service.Builder;

    public class ProductRepository : Repository
    {
        public Product Add(Product product)
        {
            this.Context.Product.Add(product);
            this.Context.SaveChanges();
            
            return product;
        }

        public List<Product> AddObjects(List<Product> products)
        {
            foreach (Product element in products)
            {
                this.Add(element);
            }

            return products;
        }

        public Product Edit(Product product)
        {
            this.Context.SaveChanges();

            return product;
        }

        public List<Product> List()
        {
            return this.Context.Product.ToList();
        }

        public void Delete(int id)
        {
            Product product = this.GetProduct(id);
            this.Context.Product.Remove(product);
            this.Context.SaveChanges();
        }

        public List<Product> Search(String search)
        {
            return this.Context.Product.Where(product => product.Code.Contains(search) || search == null).ToList();
        }

        public Product GetProduct(int id)
        {
            return this.Context.Product.Where(product => product.Id == id).First();
        }

        public int GetStock(String code)
        {
            List<Product> products = this.Context.Product.ToList();
            return products.Where(product => product.Code == code).Select(product => product.Stock).First();
        }
    }
}
