﻿using Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Console
{
    using Model.Entity.Client;
    using Model.Entity.Product;
    using Model.Entity.Command;
    using Model.Repository.Client;
    using Model.Repository.Product;
    using Model.Repository.Command;
    using Model.Service.Builder;

    class Program
    {
        static void Main(string[] args)
        {
            Context context = new Context();

            context.Database.Delete();

            // Init DB
            context.Client.ToList();
            
            context.Command.ToList();
            context.CommandProduct.ToList();
            context.Status.ToList();
            
            context.Category.ToList();
            context.LogProduct.ToList();
            context.Product.ToList();

            List<Status> status = createStatus();
            List<Category> categories = createCategories();
            List<Client> clients = createClients();
            List<Command> commands = createCommands(status, clients);
            List<Product> products  = createProducts(categories);
            List<LogProduct> logProducts = createLogProducts(products);
            List<CommandProduct> commandProducts = createCommandProducts(commands, products);
        }

        static List<Status> createStatus()
        {
            List<Status> status = StatusBuilder.createList();
            //StatusRepository repository = new StatusRepository();

            //return repository.AddObjects(status);
            return status;
        }

        static List<Category> createCategories()
        {
            List<Category> categories = CategoryBuilder.createList();
            //CategoryRepository repository = new CategoryRepository();

            //return repository.AddObjects(categories);
            return categories;
        }

        static List<Client> createClients()
        {
            List<Client> clients = ClientBuilder.createList();
            //ClientRepository repository = new ClientRepository();

            //return repository.AddObjects(clients);
            return clients;
        }

        static List<Command> createCommands(List<Status> status, List<Client> clients)
        {
            List<Command> commands = CommandBuilder.createList(status, clients);
            //CommandRepository repository = new CommandRepository();

            //return repository.AddObjects(commands);
            return commands;
        }

        static List<Product> createProducts(List<Category> categories)
        {
            List<Product> products = ProductBuilder.createList(categories);
            //ProductRepository repository = new ProductRepository();

            //return repository.AddObjects(products);
            return products;
        }

        static List<LogProduct> createLogProducts(List<Product> products)
        {
            List<LogProduct> logProducts = LogProductBuilder.createList(products);
            //LogProductRepository repository = new LogProductRepository();

            //return repository.AddObjects(logProducts);
            return logProducts;
        }

        static List<CommandProduct> createCommandProducts(List<Command> commands, List<Product> products)
        {
            List<CommandProduct> commandProducts = CommandProductBuilder.createList(commands, products);
            CommandProductRepository repository = new CommandProductRepository();

            return repository.AddObjects(commandProducts);
        }
    }
}
