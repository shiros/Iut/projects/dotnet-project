﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSiteASP.Controllers
{
    using Models.Product;
    using Model.Repository.Product;
    using Model.Entity.Product;

    public class DetailController : Controller
    {
        public ActionResult Add()
        {
            CategoryRepository categoryRepository = new CategoryRepository();

            ProductVM productVM = new ProductVM();
            productVM.Product = new Product();
            productVM.Categories = categoryRepository.List();

            return View("Add", productVM);
        }

        [HttpPost]
        public ActionResult Add(Product product)
        {
            ProductRepository productRepository = new ProductRepository();
            CategoryRepository categoryRepository = new CategoryRepository();

            product.Category = categoryRepository.GetCategory(product.CategoryId);
            productRepository.Add(product);

            return RedirectToRoute("HomePage");
        }

        public ActionResult Product(int id)
        {
            ProductRepository productRepository = new ProductRepository();
            CategoryRepository categoryRepository = new CategoryRepository();

            ProductVM productVM = new ProductVM();            
            productVM.Product = productRepository.GetProduct(id);
            productVM.Categories = categoryRepository.List();

            return View("Edit", productVM);
        }

        [HttpPost]
        public ActionResult Product(Product modifiedProduct)
        {
            ProductRepository productRepository = new ProductRepository();
            CategoryRepository categoryRepository = new CategoryRepository();

            Product product = productRepository.GetProduct(modifiedProduct.Id);

            product.Code = modifiedProduct.Code;
            product.Label = modifiedProduct.Label;
            product.Description = modifiedProduct.Description;
            product.Actif = modifiedProduct.Actif;
            product.Category = categoryRepository.GetCategory(modifiedProduct.CategoryId);

            productRepository.Edit(product);

            return RedirectToRoute("HomePage");
        }

        public ActionResult Delete(int id)
        {
            ProductRepository repository = new ProductRepository();
            repository.Delete(id);

            return RedirectToRoute("HomePage");
        }
    }
}