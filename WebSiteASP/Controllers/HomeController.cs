﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSiteASP.Controllers
{
    using Models.Product;
    using Model.Repository.Product;
    using Model.Entity.Product;

    public class HomeController : Controller
    {
        public ActionResult Index(String search)
        {
            ProductVM productVM = new ProductVM();

            ProductRepository repository = new ProductRepository();
            productVM.List = repository.Search(search);
                        
            return View(productVM);
        }
        
    }
}